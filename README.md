Senior Android Developer – AiM
==============================

The automotive industry is undergoing a revolution. Be part of it. Contribute to the next generation in software solutions for the secondary car market, touching more than 10 million transactions a year.
This is a start-up operation within an existing enterprise.
Key traits we are looking for are outstanding communication, continual learning, project/task focus, self-starter and intellectual honesty.

Experience with test driven development 
=======================================

1. [Espresso UI Test framework] (https://developer.android.com/training/testing/ui-testing/espresso-testing.html)    UI Testing

2. [Robolectric Headless DBTesting]  (http://robolectric.org/)    

3. [Teamcity Integration]  (https://www.jetbrains.com/teamcity/)  

![Vidatalk Eloquence Team City](http://www.droidaddiction.com/teamcity.png)

Developing ROI prototype solutions
==================================

Prior to developing the production application for android devices seeing here:

![Vidatalk Android Production] (https://play.google.com/store/apps/details?id=com.vidatak.vidatalk&hl=en)

I developed a prototype to present at Ohio State University as part of the requirement for a 750K grant. The prototype is located in this  private repository: https://bitbucket.org/vidatalk/vidatalk-android-prototype.  This gave us insight into a more structured approach and better design for the production application to be released in the Google Play market.  Here are some improvements we discovered:

1. Use Databases to store languages and be able to profit from language add-ons using a license server developed in JAVA Springboot (backend) and Angular ( frontend).
2. The Ipad version design needed UI improvements.  In the Android version, we used Material Design to accomplish that in the new UI approach.
3. Use a common  interface in multiple Fragments to have a single JAVA code to trigger touch events and voice events. 
4. Use material design in all the screens to simplify XML code in layouts and recyclerViews.
5. Don't use the conventional approach of resources strings for the Multi-language piece in Android devices.  

You have  but can see things with fresh eyes. You are excited to work on a fresh project without preconceived constraints and can balance that with the wisdom of experience.


Experience. Project Plan Oakland University Portal 
==================================================

1. Study new technologies and mix them with old technologies to come up 
with robust project implementations.

2. Understand your role and everyone else roles

3. Watch for deadlines (important).


![Uportal project](http://www.droidaddiction.com/uportal1.png)
![Uportal project](http://www.droidaddiction.com/uportal2.png)
![Uportal project](http://www.droidaddiction.com/uportal3.png)
![Uportal project](http://www.droidaddiction.com/uportal4.png)
![Uportal project](http://www.droidaddiction.com/uportal5.png)
![Uportal project](http://www.droidaddiction.com/uportal6.png)


Agile development (preferably SCRUM/Sprints)
============================================
![Sprints](http://www.droidaddiction.com/agile.png)
![Zephir Test](http://www.droidaddiction.com/zephirtest.png)

Self-starter, driven individual
===============================

[OTG Alliance Visual Inspection Management](https://youtu.be/DtYgbxiQk6o)

[Smart phones and Healthcare ](https://prezi.com/kpclop5az4ie/smartphones-and-healthcare/





