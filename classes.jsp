<%@ page contentType="text/html" isELIgnored="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<jsp:useBean id="gpa" scope="request" class="java.lang.String" />


<%@ taglib prefix="html" tagdir="/WEB-INF/tags/html" %>

<portlet:defineObjects/>





<table>
<tr><td>Your cumulative GPA is : <%=gpa%> </td></tr>
</table>


<table border="1">
  <tr>
    <th bgcolor="#dcdcdc">Course Name</th>
    <th bgcolor="#dcdcdc">Course Section Number</th>
    <th bgcolor="#dcdcdc">Instructor</th>
    <th bgcolor="#dcdcdc">Class Begin Time</th>
    <th bgcolor="#dcdcdc">Class End Time</th>
    <th bgcolor="#dcdcdc">Days</th>
    <th bgcolor="#dcdcdc">Building / Room</th>
  </tr>

  <c:forEach var="course" items="${courses}">
    <% int i = 0; %>
    <tr>
      <%
        String bgColor = (i % 2 == 0) ? "#ffffff" : "#dcdcdc";
      %>
      <td style="background-color:<%=bgColor%>;"><c:out value="${course.courseName}" /></td>
      <td style="background-color:<%=bgColor%>;"><c:out value="${course.courseSectionNumber}" /></td>
      <td style="background-color:<%=bgColor%>;"><c:out value="${course.instructor}" /></td>
      <td style="background-color:<%=bgColor%>;"><c:out value="${course.classBeginTime}" /></td>
      <td style="background-color:<%=bgColor%>;"><c:out value="${course.classEndTime}" /></td>
      <td style="background-color:<%=bgColor%>;"><c:out value="${course.days}" /></td>
      <td style="background-color:<%=bgColor%>;"><c:out value="${course.building}" /> <c:out value="${course.room}" /></td>
    </tr>
    <% i++; %>
  </c:forEach>
</table>

<%
  String  term_desc;
  String  w_xml_url   = "http://oakland.bncollege.com/webapp/wcs/stores/servlet/TBListView";
  String  w_noxml_url   = "http://oakland.bncollege.com/webapp/wcs/stores/servlet/TBWizardView";
  String  w_store_id      = "13551";
  String  w_school_id     = "244";
  String  w_catalog_id    = "10001";
  String  w_lang_id       = "-1";
  String  w_term_mapping  = "N";
  String  w_xml           ;
  String  w_have_any      ;
  String  w_use_url       = "http://oakland.bncollege.com/webapp/wcs/stores/servlet/TBWizardView";

%>
<c:choose>



  <c:when test="${coursesWithBooks != null}">
    <%
        out.print("<form name=BNform method='post' action='" + w_xml_url + "'>");
        out.print("<input type=hidden name=storeId value='" + w_store_id + "'/>");
        out.print("<input type=hidden name=catalogId value='" + w_catalog_id + "'/>");
        out.print("<input type=hidden name=langId value='" + w_lang_id + "'/>");
       out.print("<input type=hidden name=termMapping value='" + w_term_mapping + "' />");
       out.print("<input type=hidden name=courseXml value=\"<?xml version='1.0' encoding='UTF-8'?>" +
      "<textbookorder><school id='" + w_school_id + "'/><courses>");
    %>
   <c:forEach var="course" items="${coursesWithBooks}">


     <% out.print("<course dept='"); %> <c:out value="${course.subject}" />
     <% out.print("' num='") ;      %> <c:out value="${course.course}" />
     <% out.print("' sect='") ;     %> <c:out value="${course.section}" />
     <% out.print("' term='")  ;    %> <c:out value="${course.term}" />
     <% out.print("' /> "); %>

    </c:forEach>
    <%
      out.println("</courses></textbookorder> \" />");
    %>
  </c:when>
<c:otherwise>
      out.print("<form name=BNform method='post' action='" + w_noxml_url + "'>");
      out.print("<input type=hidden name=storeId value='" + w_store_id + "'/>");
      out.print("<input type=hidden name=catalogId value='" + w_catalog_id + "'/>");
      out.print("<input type=hidden name=langId value='" + w_lang_id + "'/>");

    <% out.print("<input type=hidden name=level value='1'/>"); %>

  </c:otherwise>
</c:choose>

<input type=submit value='Buy my books'>
</form>
