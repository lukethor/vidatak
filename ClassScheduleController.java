package edu.oakland.my.portlets.classschedule.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;



import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.oakland.my.portlets.classschedule.Constants;
import edu.oakland.my.portlets.classschedule.model.Course;
import edu.oakland.my.portlets.classschedule.model.CourseWithBooks;
import edu.oakland.my.portlets.classschedule.model.CourseGrades;
import edu.oakland.my.portlets.classschedule.model.CourseTaken;


@Controller
@RequestMapping("VIEW")
public class ClassScheduleController {
 


private String getCoursesCountSql = " SELECT count(SSBSECT_SUBJ_CODE||'-'||SSBSECT_CRSE_NUMB) "  +
" FROM SATURN.SFRSTCR,SATURN.SSBSECT " +
" WHERE SFRSTCR_PIDM = ? " +
"	AND SFRSTCR_TERM_CODE = ( SELECT  GTVSDAX_TRANSLATION_CODE " +
"					  FROM GTVSDAX, STVTERM " +
"					  WHERE GTVSDAX_INTERNAL_CODE = 'PORTAL' " +
"					  AND GTVSDAX_INTERNAL_CODE_GROUP = 'GRDTERM' " +
"					/* AND GTVSDAX_INTERNAL_CODE_GROUP = 'CURTERM' */ " +
 "     				  AND STVTERM_CODE = GTVSDAX_TRANSLATION_CODE ) " +
"	AND SFRSTCR_RSTS_CODE IN ('RW','RE') " +
"	AND SSBSECT_TERM_CODE = SFRSTCR_TERM_CODE " +
"	AND SSBSECT_CRN = SFRSTCR_CRN " +
"	ORDER BY SSBSECT_SUBJ_CODE, SSBSECT_CRSE_NUMB, SSBSECT_SEQ_NUMB ";



 private String getCoursesTakenSql = "SELECT TO_CHAR(SSBSECT_SUBJ_CODE||'-'||SSBSECT_CRSE_NUMB) as description,  sfrstcr_crn AS crn " +
" FROM SATURN.SFRSTCR,SATURN.SSBSECT " +
" WHERE SFRSTCR_PIDM = ? " +
	" AND SFRSTCR_TERM_CODE =( SELECT  GTVSDAX_TRANSLATION_CODE " +
					 " FROM GTVSDAX, STVTERM " +
					 " WHERE GTVSDAX_INTERNAL_CODE = 'PORTAL' " +
					 " AND GTVSDAX_INTERNAL_CODE_GROUP = 'GRDTERM' " +
      			"	  AND STVTERM_CODE = GTVSDAX_TRANSLATION_CODE ) " +
"	AND SFRSTCR_RSTS_CODE IN ('RW','RE') " +
"	AND SSBSECT_TERM_CODE = SFRSTCR_TERM_CODE " +
"	AND SSBSECT_CRN = SFRSTCR_CRN " +
"	ORDER BY SSBSECT_SUBJ_CODE, SSBSECT_CRSE_NUMB, SSBSECT_SEQ_NUMB ";



private String getGradesSql = " SELECT SHRTCKG_GRDE_CODE_FINAL AS grade,SHRTCKN_CRN AS crn " +
"FROM SATURN.SHRTCKG,SATURN.SHRTCKN " +
"WHERE SHRTCKN_PIDM = ? " +
"	/* AND SHRTCKN_CRN = CRN */ " +
"	AND SHRTCKN_TERM_CODE = ( SELECT  GTVSDAX_TRANSLATION_CODE " +
"					  FROM GTVSDAX, STVTERM " +
"					  WHERE GTVSDAX_INTERNAL_CODE = 'PORTAL' " +
"					  AND GTVSDAX_INTERNAL_CODE_GROUP = 'GRDTERM' " +
"					/* AND GTVSDAX_INTERNAL_CODE_GROUP = 'CURTERM' */ " +
"      				  AND STVTERM_CODE = GTVSDAX_TRANSLATION_CODE ) " +
"	AND SHRTCKG_PIDM = SHRTCKN_PIDM " +
"	AND SHRTCKG_TERM_CODE = SHRTCKN_TERM_CODE " +
"	AND SHRTCKG_TCKN_SEQ_NO = SHRTCKN_SEQ_NO " +
"	AND SHRTCKG_SEQ_NO = " +
"            (SELECT MAX(b.shrtckg_seq_no) FROM SATURN.shrtckg b " +
"           	WHERE b.shrtckg_pidm = shrtckn_pidm " +
"           		AND   b.shrtckg_term_code = shrtckn_term_code " +
"           		AND   b.shrtckg_tckn_seq_no = shrtckn_seq_no) " ;



private String getGpaSql =  " SELECT /*SHRLGPA_HOURS_ATTEMPTED,SHRLGPA_HOURS_EARNED,SHRLGPA_GPA_HOURS,*/ " +
	" /*SHRLGPA_QUALITY_POINTS,*/ TO_CHAR(SHRLGPA_GPA) " +
    " FROM saturn.SHRLGPA " +
    " WHERE SHRLGPA_PIDM = :id " + 
	" AND SHRLGPA_LEVL_CODE = ( SELECT SGBSTDN_LEVL_CODE " +
    " FROM saturn.SGBSTDN " +
    " WHERE  SGBSTDN_PIDM = :id " +
    " AND SGBSTDN_TERM_CODE_EFF <= ( SELECT GTVSDAX_TRANSLATION_CODE FROM GTVSDAX, STVTERM  " +
                                                                 " WHERE GTVSDAX_INTERNAL_CODE = 'PORTAL' " + 
                                                                 " AND GTVSDAX_INTERNAL_CODE_GROUP = 'CURTERM' " +
                                                                 " AND STVTERM_CODE = GTVSDAX_TRANSLATION_CODE " +
    "                            ) " +
    " AND SGBSTDN_TERM_CODE_EFF = ( SELECT MAX(SGBSTDN_TERM_CODE_EFF) " + 
                              "  FROM saturn.SGBSTDN " + 
                              "  WHERE     SGBSTDN_PIDM = :id " +
                              "  AND SGBSTDN_TERM_CODE_EFF <= ( SELECT GTVSDAX_TRANSLATION_CODE " + 
                                                              " FROM GTVSDAX, STVTERM  " +
                                                              " WHERE GTVSDAX_INTERNAL_CODE = 'PORTAL'  " +
                                                              " AND GTVSDAX_INTERNAL_CODE_GROUP = 'CURTERM'  " +
                                                              " AND STVTERM_CODE = GTVSDAX_TRANSLATION_CODE ) " + 
 "                                ) ) " +
 "	AND SHRLGPA_GPA_TYPE_IND = 'I' " ;	
	

  private String getPidmSql = "SELECT GOREMAL_PIDM FROM GOREMAL WHERE GOREMAL_EMAL_CODE = 'OAKU' " +
                    "AND SUBSTR(UPPER(goremal_email_address),1,instr(UPPER(goremal_email_address),'@OAKLAND.EDU',1)-1)= upper(?)"; //username
  private String getScheduleSql = "SELECT  SCBCRSE_TITLE CRS_TITLE," +
                    "SSBSECT_SUBJ_CODE || '-' || SSBSECT_CRSE_NUMB || '-' || SSBSECT_SEQ_NUMB CRS_INFO," +
                    "SPRIDEN_LAST_NAME || ', ' || substr(SPRIDEN_FIRST_NAME, 1, 1) CRS_INSTR," +
                    "SFRSTCR_CRN CRS_CRN," +
                    "to_char(to_date(SSRMEET_BEGIN_TIME, 'hh24:mi'), 'hh12:mi A.M.') CRS_BEGIN," +
                    "to_char(to_date(SSRMEET_END_TIME, 'hh24:mi'), 'hh12:mi A.M.') CRS_END," +
                    "SSRMEET_MON_DAY || SSRMEET_TUE_DAY || SSRMEET_WED_DAY || SSRMEET_THU_DAY || SSRMEET_FRI_DAY || SSRMEET_SAT_DAY MEET_DAYS," +
                    "SSRMEET_BLDG_CODE BLDG," +
                    "SSRMEET_ROOM_CODE ROOM " +
                    "FROM SFRSTCR," +
                    "SCBCRSE A," +
                    "SSBSECT," +
                    "SIRASGN," +
                    "SSRMEET," +
                    "SPRIDEN " +
                    "WHERE SFRSTCR_PIDM = ? " + //thepidm
                    "AND SFRSTCR_TERM_CODE = (SELECT GTVSDAX_TRANSLATION_CODE " +
                                              "FROM GTVSDAX," +
                                              "STVTERM " +
                                              "WHERE GTVSDAX_INTERNAL_CODE = 'PORTAL' " +
                                              "AND GTVSDAX_INTERNAL_CODE_GROUP = 'CURTERM' " +
                                              "AND STVTERM_CODE = GTVSDAX_TRANSLATION_CODE) " +
                    "AND SFRSTCR_RSTS_CODE IN ('RW', 'RE', 'AU', 'RR') " +
                    "AND SSBSECT_CRN = SFRSTCR_CRN " +
                    "AND SSBSECT_CRN = SIRASGN_CRN " +
                    "AND SSBSECT_TERM_CODE = SFRSTCR_TERM_CODE " +
                    "AND SSBSECT_TERM_CODE = SIRASGN_TERM_CODE " +
                    "AND SSRMEET_TERM_CODE = SFRSTCR_TERM_CODE " +
                    "AND SCBCRSE_SUBJ_CODE = SSBSECT_SUBJ_CODE " +
                    "AND SCBCRSE_CRSE_NUMB = SSBSECT_CRSE_NUMB " +
                    "AND SSRMEET_CRN = SFRSTCR_CRN " +
                    "AND SPRIDEN_PIDM = SIRASGN_PIDM " +
                    "AND SIRASGN_PRIMARY_IND = 'Y' " +
                    "AND SPRIDEN_CHANGE_IND IS NULL " +
                    "AND SCBCRSE_EFF_TERM = (SELECT MAX(SCBCRSE_EFF_TERM) " +
                                             "FROM SCBCRSE B " +
                                             "WHERE A.SCBCRSE_SUBJ_CODE = B.SCBCRSE_SUBJ_CODE " +
                                             "AND A.SCBCRSE_CRSE_NUMB = B.SCBCRSE_CRSE_NUMB " +
                                             "AND A.SCBCRSE_EFF_TERM <= B.SCBCRSE_EFF_TERM) ";

  private String hasBooksSql = "select NVL(MAX('Y'),'N') have_any from sfrstcr, stvterm " +
                    "where sfrstcr_pidm = ? " + //thepidm
                    "and stvterm_code = sfrstcr_term_code and trunc(stvterm_end_date) > trunc(sysdate)";
  
  private String getBooksSql = "select decode(substr(ssbsect_term_code,5,1),'4','F','2','S','3','A','1','W') || " +
                    "substr(ssbsect_term_code,3,2) TERMX, " +
                    "ssbsect_subj_code SUBJ, " +
                    "ssbsect_crse_numb CRSE, " +
                    "ssbsect_crn SECT " +
                    "from sfrstcr, stvterm, ssbsect " +
                    "where sfrstcr_pidm = ? " + //thepidm
                    "and stvterm_code = sfrstcr_term_code and trunc(stvterm_end_date) > trunc(sysdate) " +
                    "and ssbsect_term_code = sfrstcr_term_code and ssbsect_crn = sfrstcr_crn " +
                    "order by ssbsect_term_code, SUBJ, CRSE, SECT";

  /** Instance of Commons Logging. */
  private static final Log log = LogFactory.getLog(ClassScheduleController.class);
  private SimpleJdbcTemplate jdbcTemplate;
  
  public void setDataSource(DataSource ds) {
    this.jdbcTemplate = new SimpleJdbcTemplate(ds);
  }

  @RequestMapping  // default view of the portlet
  public String showSchedule (PortletRequest request, Model model) {
    String pidm = getPidm(request);

        String gpa = getGpa(pidm);
        gpa = gpa.substring(0,4);
        model.addAttribute("gpa",gpa); 

        
        

        
    List<Course> courses = getCourseList(pidm);
    model.addAttribute("courses", courses);
    
    boolean hasBooks = getHasBooks(pidm);
    
    if (hasBooks) {
      List<CourseWithBooks> coursesWithBooks = getCoursesWithBooks(pidm);
      model.addAttribute("coursesWithBooks", coursesWithBooks);
    }
    
    // now get the grades for previous courses taken
    
    Integer TheCourseCounter = getCourseCount(pidm);
    model.addAttribute("TheCourseCounter",TheCourseCounter);
    
    List<CourseGrades> coursegrade = getGradeList(pidm);
    model.addAttribute("coursegrade", coursegrade);
    
    List<CourseTaken> coursetaken =  getCourseTakenList(pidm);
    model.addAttribute("coursetaken", coursetaken);
    
    
    return "classes";
	}

  private List<CourseWithBooks> getCoursesWithBooks(String pidm) {
    ParameterizedRowMapper<CourseWithBooks> mapper = new ParameterizedRowMapper<CourseWithBooks>(){

      public CourseWithBooks mapRow(ResultSet rs, int rowNumber) throws SQLException {
        CourseWithBooks course = new CourseWithBooks();
        course.setTerm(rs.getString("TERMX"));
        course.setSubject(rs.getString("SUBJ"));
        course.setCourse(rs.getString("CRSE"));
        course.setSection(rs.getString("SECT"));
        return course; 
      }
    };
    List<CourseWithBooks> courses = jdbcTemplate.query(getGetBooksSql(), mapper, pidm);
    return courses;
  }

  private boolean getHasBooks(String pidm) {
    boolean hasBooks = false;
    String hasBooksFlag = jdbcTemplate.queryForObject(getHasBooksSql(), String.class, pidm);

    if (hasBooksFlag.equals("Y"))
      hasBooks = true;

    return hasBooks;
  }

  private String getPidm(PortletRequest request) {
    PortletSession session = request.getPortletSession();
    String pidm = (String)session.getAttribute(Constants.PIDM_SESSION_KEY);
    
    // Retrieve from the database if not cached
    if (pidm == null) {
      String userName = getUserName(request);
      pidm = jdbcTemplate.queryForObject(getGetPidmSql(), String.class, userName);
      session.setAttribute(Constants.PIDM_SESSION_KEY, pidm);
    }
    return pidm;
  }
  

  private String getGpa(String pidm) {
   SqlParameterSource namedParameters = new MapSqlParameterSource("id", pidm);
   String MyGpa =  jdbcTemplate.queryForObject(getGpaSql(), String.class, namedParameters);
   return MyGpa;
	  }
  
  private Integer getCourseCount(String pidm) {
	   String CourseCounter =  jdbcTemplate.queryForObject(getCoursesCountSql(), String.class, pidm);
	   Integer TheCourseCounter = Integer.valueOf(CourseCounter);
	   return TheCourseCounter;
		  }
  
  
  
  private List<CourseTaken> getCourseTakenList(String pidm) {
    ParameterizedRowMapper<CourseTaken> mapper = new ParameterizedRowMapper<CourseTaken>(){

      public CourseTaken mapRow(ResultSet rs, int rowNumber) throws SQLException {
        CourseTaken coursetaken = new CourseTaken();
        coursetaken.setDescription(rs.getString("description"));
        coursetaken.setCrn(rs.getString("crn"));
        return coursetaken;
      }
    };
    List<CourseTaken> coursetaken = jdbcTemplate.query(getGetCoursesTakenSql(), mapper, pidm);
    return coursetaken;
  }



  
  private List<Course> getCourseList(String pidm) {
    ParameterizedRowMapper<Course> mapper = new ParameterizedRowMapper<Course>(){

      public Course mapRow(ResultSet rs, int rowNumber) throws SQLException {
        Course course = new Course();
        course.setCourseName(rs.getString("CRS_TITLE"));
        course.setCourseSectionNumber(rs.getString("CRS_INFO"));
        course.setInstructor(rs.getString("CRS_INSTR"));
        course.setClassBeginTime(rs.getString("CRS_BEGIN"));
        course.setClassEndTime(rs.getString("CRS_END"));
        course.setDays(rs.getString("MEET_DAYS"));
        course.setBuilding(rs.getString("BLDG"));
        course.setRoom(rs.getString("ROOM"));
        return course;
      }
    };
    List<Course> courses = jdbcTemplate.query(getGetScheduleSql(), mapper, pidm);
    return courses;
  }



  private List<CourseGrades> getGradeList(String pidm) {
	  
    ParameterizedRowMapper<CourseGrades> mapper = new ParameterizedRowMapper<CourseGrades>(){

      public CourseGrades mapRow(ResultSet rs, int rowNumber) throws SQLException {
    	  CourseGrades coursegrade = new CourseGrades();
        coursegrade.setGrade(rs.getString("grade"));
        coursegrade.setCrn(rs.getString("crn"));
        return coursegrade;
      }
    };
    List<CourseGrades> coursegrade = jdbcTemplate.query(getGetGradesSql(), mapper, pidm);
    return coursegrade;
  }



  
  

  private String getUserName(PortletRequest request) {
//    return "jbsulliv";
//    return "szalay";
 //   return "wjham";
    
    String userName = request.getRemoteUser();
    return userName;
  }

  /**
   * @return the getPidmSql
   */
  public String getGetPidmSql() {
    return this.getPidmSql;
  }

  /**
   * @param getPidmSql the getPidmSql to set
   */
  public void setGetPidmSql(String getPidmSql) {
    this.getPidmSql = getPidmSql;
  }

  /**
   * @return the getScheduleSql
   */
  public String getGetScheduleSql() {
    return this.getScheduleSql;
  }

  /**
   * @param getScheduleSql the getScheduleSql to set
   */
  public void setGetScheduleSql(String getScheduleSql) {
    this.getScheduleSql = getScheduleSql;
  }

  /**
   * @return the getGradesSql
   */
  public String getGetGradesSql() {
    return this.getGradesSql;
  }

  /**
   * @param geGradesSql the getGradesSql to set
   */
  public void setGetGradesSql(String getGradesSql) {
    this.getGradesSql = getGradesSql;
  }

  
  /**
   * @return the getCoursesTakenSql
   */
  public String getGetCoursesTakenSql() {
    return this.getCoursesTakenSql;
  }

  /**
   * @param getGetCoursesTaken to set
   */
  public void setGetCousesTakenSql(String getCoursesTakenSql) {
    this.getCoursesTakenSql = getCoursesTakenSql;
  }
  
  
  
  
  /**
   * @return the hasBooksSql
   */
  public String getHasBooksSql() {
    return this.hasBooksSql;
  }

  /**
   * @param hasBooksSql the hasBooksSql to set
   */
  public void setHasBooksSql(String hasBooksSql) {
    this.hasBooksSql = hasBooksSql;
  }

  /**
   * @return the getBooksSql
   */
  public String getGetBooksSql() {
    return this.getBooksSql;
  }

  /**
   * @param getBooksSql the getBooksSql to set
   */
  public void setGetBooksSql(String getBooksSql) {
    this.getBooksSql = getBooksSql;
  }

  /**
   * @param getGpa the getGpaSql to set
   */
  public String getGpaSql() {
    return this.getGpaSql;
  }
  
  /**
   * @param getGpa the getGpaSql to set
   */
  public void setGetGpaSql(String getGpaSql) {
    this.getGpaSql = getGpaSql;
  }


  public String getCoursesCountSql() {
	    return this.getCoursesCountSql;
	  }
	  
	  /**
	   * @param getGpa the getGpaSql to set
	   */
  public void setGetCoursesCountSql(String getCoursesCountSql) {
	    this.getCoursesCountSql = getCoursesCountSql;
	  }
  
  
}
